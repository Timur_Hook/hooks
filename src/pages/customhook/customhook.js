import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native';

function useInput(initialState) {
    const [value, setValue] = useState(initialState);
    const reset = () => setValue(initialState);


    const bind = {
        value,
        onChangeText: (text) => setValue(text)
    }
    return [value, bind, reset];
}




const Customhook = () => {
    const [name, bindName, resetName] = useInput('');
    const navigation = useNavigation();


    const {buttonStyle, inputStyle} = styles;
    return (
    <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
        <TouchableOpacity 
            style={buttonStyle}
            onPress={() => navigation.goBack()}>
            <Text style={{textAlign: 'center'}}>Назад</Text>
        </TouchableOpacity>
        <Text>{name}</Text>
        <TextInput  
                {...bindName}
                style={inputStyle}
            />
        <TouchableOpacity 
            style={buttonStyle}
            onPress={resetName}>
            <Text style={{textAlign: 'center'}}>Очистить</Text>
        </TouchableOpacity>
    </View>
    )
}



const styles = StyleSheet.create({
    buttonStyle: {width:'80%', height: 40, borderWidth: 1, borderColor: 'blue', borderRadius: 5, justifyContent: 'center'},
    inputStyle: {
        height: 40,
        width: '80%',
        borderWidth: 1,
        borderColor: 'red',
        textAlign: 'center',
        marginTop: 40
    }
});

export default Customhook;