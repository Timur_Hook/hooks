import React, {useState, useEffect, useReducer} from 'react';
import {View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const INITIAL_STATE = 0;

const reducer = (state, action) => {
    switch(action) {
        case 'INCREMENT':  return state + 1;
        case 'DECREMENT':  return state - 1;
        case 'RESET': return 0;
        default: return state;
    }
}

const Usereducer = () => {
    const navigation = useNavigation();
    const [count, dispatchEvent] = useReducer(reducer, INITIAL_STATE);
    const {buttonStyle, inputStyle} = styles;
return (
   <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => navigation.goBack()}>
           <Text style={{textAlign: 'center'}}>Назад</Text>
       </TouchableOpacity>
       <Text style={{textAlign: 'center', fontSize: 25}}>{count}</Text>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => dispatchEvent('INCREMENT')}>
           <Text style={{textAlign: 'center'}}>Инкрементировать</Text>
       </TouchableOpacity>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => dispatchEvent('DECREMENT')}>
           <Text style={{textAlign: 'center'}}>Декрементировать</Text>
       </TouchableOpacity>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => dispatchEvent('RESET')}>
           <Text style={{textAlign: 'center'}}>Обнулить</Text>
       </TouchableOpacity>
   </View>
 )
}

const styles = StyleSheet.create({
    buttonStyle: {width:'80%', height: 40, borderWidth: 1, borderColor: 'blue', borderRadius: 5, justifyContent: 'center'},
});

export default Usereducer;