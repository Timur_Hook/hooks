import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const Usestate = () => {
    const [counter, setCounter] = useState(0);
    const navigation = useNavigation();


    // const increment = () => setCounter((previous) => previous + 1)
    // const decrement = () => setCounter((previous) => previous - 1)

    const increment2 = () => setCounter(counter + 3)
    const decrement2 = () => setCounter(counter  - 3)

    const {buttonStyle} = styles;

    
return (
   <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => navigation.goBack()}>
           <Text style={{textAlign: 'center'}}>Назад</Text>
       </TouchableOpacity>

       <Text style={{textAlign: 'center', fontSize: 40}}>{counter}</Text>

       <TouchableOpacity 
        style={buttonStyle}
        onPress={increment2}>
           <Text style={{textAlign: 'center', fontSize: 40}}>+</Text>
       </TouchableOpacity>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={decrement2}>
           <Text style={{textAlign: 'center', fontSize: 40}}>-</Text>
       </TouchableOpacity>
       
   </View>
 )
}

const styles = StyleSheet.create({
    buttonStyle: {width:'80%', height: 40, borderWidth: 1, borderColor: 'blue', borderRadius: 5, justifyContent: 'center'}
});

export default Usestate;