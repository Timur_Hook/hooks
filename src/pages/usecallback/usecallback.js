import React, {useState, useEffect, useMemo, useCallback} from 'react';
import {View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import CustomCounter from './count';
import ButtonCustom from './customButton';

const Usecallback = () => {
    const [counterOne, setCounterOne] = useState(25);
    const [counterTwo, setCounterTwo] = useState(1000000);
    const navigation = useNavigation();

    const {buttonStyle, inputStyle} = styles;

    // const incrementCounterOne = () => {
    //     setCounterOne(counterOne + 1)
    // }

    // const incrementCounterTwo = () => {
    //     setCounterTwo(counterTwo + 50000)
    // }

    const incrementCounterOne = useCallback(() => {
        setCounterOne(counterOne + 1)
    }, [counterOne])

    const incrementCounterTwo = useCallback(() => {
        setCounterTwo(counterTwo + 50000)
    }, [counterTwo])

return (
   <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => navigation.goBack()}>
           <Text style={{textAlign: 'center'}}>Назад</Text>
       </TouchableOpacity>

       <CustomCounter text="Возраст" count={counterOne}/>
       <ButtonCustom label="Возраст" handlePress={incrementCounterOne}/>

       <CustomCounter text="Зарплата" count={counterTwo}/>
       <ButtonCustom label="Зарплата" handlePress={incrementCounterTwo}/>
       </View>
 )
}

const styles = StyleSheet.create({
    buttonStyle: {width:'80%', height: 40, borderWidth: 1, borderColor: 'blue', borderRadius: 5, justifyContent: 'center'},
    inputStyle: {
        height: 40,
        width: '80%',
        borderWidth: 1,
        borderColor: 'red',
        textAlign: 'center'
    }
});

export default Usecallback;