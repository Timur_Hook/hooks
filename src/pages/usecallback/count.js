import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'

const CustomCounter = ({text, count}) => {

   const {
     } = styles

   return (<View style={{width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection:'row'}}>
       <Text style={{textAlign: 'center', fontSize: 20, marginRight: 40}}>{text}</Text>
       <Text style={{textAlign: 'center', fontSize: 20}}>{count}</Text>
    </View>)
}

const styles = StyleSheet.create({})

export default React.memo(CustomCounter)