import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'

const ButtonCustom = ({handlePress, label }) => {
    console.log('%c++ =====', 'background:red', label);
   const {
     } = styles

   return (<TouchableOpacity onPress={handlePress} style={{height: 40, width: '80%', backgroundColor: 'blue', justifyContent: 'center'}}>
       <Text style={{textAlign: 'center', color: '#fff', fontSize: 25}}>{label}</Text>
   </TouchableOpacity>)
}

const styles = StyleSheet.create({})

export default React.memo(ButtonCustom)