import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {ComponentOne} from './components/componentOne';
export const UserContext = React.createContext();
export const CounterContext = React.createContext();

const Usecontext = () => {
    const [counter, setCounter] = useState(0)

    const navigation = useNavigation();
    const {buttonStyle} = styles;
return (
   <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
       <Text style={{textAlign: 'center', fontSize: 20}}>Самый верхний компонент</Text>
       <UserContext.Provider value={{id: 1, name: 'Hook', lastname: 'Banner'}}>
           <CounterContext.Provider value={{counter}} >
            <ComponentOne />
           </CounterContext.Provider>
        </UserContext.Provider>
        <TouchableOpacity 
            style={buttonStyle}
            onPress={() => setCounter(counter + 4)}>
            <Text style={{textAlign: 'center'}}>Добавить</Text>
        </TouchableOpacity>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => navigation.goBack()}>
           <Text style={{textAlign: 'center'}}>Назад</Text>
       </TouchableOpacity>
       <Text>Hello World</Text>
   </View>
 )
}

const styles = StyleSheet.create({
    buttonStyle: {width:'80%', height: 40, borderWidth: 1, borderColor: 'blue', borderRadius: 5, justifyContent: 'center'}
});

export default Usecontext;