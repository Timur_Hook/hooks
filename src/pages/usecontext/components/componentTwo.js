import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import { ComponentThree } from './componentThree';

const ComponentTwo = () => {

   const {
     } = styles

   return (<View style={{width: '90%', height: 500, backgroundColor: 'violet', justifyContent: 'center', alignItems: 'center'}}>
       <Text style={{textAlign: 'center', fontSize: 35}}>Компонент №2</Text>
       <ComponentThree />
   </View>)
}

const styles = StyleSheet.create({})

export {ComponentTwo}