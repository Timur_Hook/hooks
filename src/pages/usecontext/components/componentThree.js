import React, {useContext} from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import {UserContext, CounterContext} from './../usecontext';

const ComponentThree = () => {
    const user = useContext(UserContext);
    const counter = useContext(CounterContext);
   const {
     } = styles

   return (<View style={{
                    width:'80%', 
                    height: 400, 
                    backgroundColor: 'lime',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Text style={{textAlign: 'center', fontSize: 35}}>Компонент №3</Text>
                <Text style={{textAlign: 'center', fontSize: 20}}>{`${user.name} ${user.lastname}`}</Text>
                <Text style={{textAlign: 'center', fontSize: 20}}>{counter.counter}</Text>
            </View>)
}

const styles = StyleSheet.create({})

export {ComponentThree}