import React from 'react'
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import {ComponentTwo} from './componentTwo';

const ComponentOne = () => {

   const {
     } = styles

   return (<View 
            style={{height: 600, width: '100%', backgroundColor: 'orange', justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{textAlign: 'center', fontSize: 35}}>Компонент №1</Text>
       <ComponentTwo />
   </View>)
}

const styles = StyleSheet.create({})

export {ComponentOne}