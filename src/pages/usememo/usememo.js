import React, {useState, useEffect, useMemo} from 'react';
import {View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const Usememo = () => {
    const [counterOne, setCounterOne] = useState(0);
    const [counterTwo, setCounterTwo] = useState(0);
    const navigation = useNavigation();

    const {buttonStyle, inputStyle} = styles;

    // const isEven = () => {
    //     let i = 0;
    //     while(i < 1000000000) {
    //         i++;
    //     }
    //     return counterOne % 2
    // }

    const isEven = useMemo(() => {
        let i = 0;
        while(i < 1000000000) {
            i++;
        }
        return counterOne % 2
    }, [counterOne])

return (
   <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => navigation.goBack()}>
           <Text style={{textAlign: 'center'}}>Назад</Text>
       </TouchableOpacity>
            <Text style={{marginTop: 30, fontSize: 25}}>{isEven ? 'Even' : 'Odd'} - {counterOne}</Text>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => setCounterOne(counterOne + 1)}>
           <Text style={{textAlign: 'center'}}>INCREMENT ONE</Text>
       </TouchableOpacity>
        <View style={{marginTop: 30, justifyContent: 'center', width: '100%', alignItems: 'center'}}>
        <Text style={{textAlign: 'center', fontSize: 25}}>{counterTwo}</Text>
        <TouchableOpacity 
            style={buttonStyle}
            onPress={() => setCounterTwo(counterTwo + 1)}>
            <Text style={{textAlign: 'center'}}>INCREMENT TWO</Text>
        </TouchableOpacity>
       </View>
   </View>
 )
}

const styles = StyleSheet.create({
    buttonStyle: {width:'80%', height: 40, borderWidth: 1, borderColor: 'blue', borderRadius: 5, justifyContent: 'center'},
    inputStyle: {
        height: 40,
        width: '80%',
        borderWidth: 1,
        borderColor: 'red',
        textAlign: 'center'
    }
});

export default Usememo;