import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const Useeffect = () => {
    const [counter, setCounter] = useState(0);
    const [rtr, setRtr] =useState(12)
    const [data, setData] = useState(0)

    useEffect(() => {
        console.log('%c++[        ]','background: violet', 'Будет срабатывать всегда, потому что не указали второй параметр');
    })

    useEffect(() => {
        console.log('%c++[        ]','background: tomato', 'Сработает только один раз. При рендеринге компонента');
    }, [counter])

    useEffect(() => {
        console.log('%c++[        ]','background: lime', 'Будет срабатывать при каждом изменении counter');
    }, [counter])

    const increment = () => setCounter((previous) => previous + 1)
    const increment2 = () => setData((previous) => previous + 1)

    const navigation = useNavigation();
    const {buttonStyle} = styles;
return (
   <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
       <TouchableOpacity 
        style={{width:'80%', height: 40, borderWidth: 1, borderColor: 'blue', borderRadius: 5, justifyContent: 'center'}}
        onPress={() => navigation.goBack()}>
           <Text style={{textAlign: 'center'}}>Назад</Text>
       </TouchableOpacity>
       <Text style={{textAlign: 'center', fontSize: 40}}>{counter}</Text>

       <TouchableOpacity 
        style={buttonStyle}
        onPress={increment}>
           <Text style={{textAlign: 'center', fontSize: 40}}>+</Text>
       </TouchableOpacity>

       <TouchableOpacity 
        style={buttonStyle}
        onPress={increment2}>
           <Text style={{textAlign: 'center', fontSize: 20}}>Изменяем другую переменную</Text>
       </TouchableOpacity>
   </View>
 )
}

const styles = StyleSheet.create({
    buttonStyle: {width:'80%', height: 40, borderWidth: 1, borderColor: 'blue', borderRadius: 5, justifyContent: 'center'}
});

export default Useeffect;