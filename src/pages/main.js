import React, {useState, useEffect} from 'react';
import {View, Text, Button, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const MainPage = () => {
    const navigation = useNavigation();
    return (
    <View style={{flex: 1, justifyContent: 'center', alignItems:'center'}}>
        <Button title="useState" onPress={() => navigation.navigate('Usestate')} />
        <Button title="useEffect" onPress={() => navigation.navigate('Useeffect')} />
        <Button title="useRef" onPress={() => navigation.navigate('Useref')} />
        <Button title="useContext" onPress={() => navigation.navigate('Usecontext')} />
        <Button title="useReducer" onPress={() => navigation.navigate('Usereducer')} />
        <Button title="useMemo" onPress={() => navigation.navigate('Usememo')} />
        <Button title="useCallback" onPress={() => navigation.navigate('Usecallback')} />
        <Button title="Custom Hook" onPress={() => navigation.navigate('Customhook')} />
    </View>
    )
}

const styles = StyleSheet.create({});

export default MainPage;