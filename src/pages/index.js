import React, {useState, useEffect} from 'react';
import {View, Text, Button, StyleSheet } from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import MainPage from './main';
import Usestate from './usestate/usestate';
import Useref from './useref/useref';
import Useeffect from './useeffect/useeffect';
import Usecontext from './usecontext/usecontext';
import Usereducer from './usereducer/usereducer';
import Usememo from './usememo/usememo';
import Usecallback from './usecallback/usecallback';
import Customhook from './customhook/customhook';

const Stack = createStackNavigator();


const MainStack = () => {
  return (
    <Stack.Navigator initialRouteName="MainPage" headerMode="none">
      <Stack.Screen name="MainPage" component={MainPage} />
      <Stack.Screen name="Usestate" component={Usestate} />
      <Stack.Screen name="Useref" component={Useref} />
      <Stack.Screen name="Useeffect" component={Useeffect} />
      <Stack.Screen name="Usecontext" component={Usecontext} />
      <Stack.Screen name="Usereducer" component={Usereducer} />
      <Stack.Screen name="Usememo" component={Usememo} />
      <Stack.Screen name="Usecallback" component={Usecallback} />
      <Stack.Screen name="Customhook" component={Customhook} />
    </Stack.Navigator>
  )
}


const RootComponent = () => {
return (
  <NavigationContainer>
    <MainStack />
  </NavigationContainer>
 )
}

const styles = StyleSheet.create({});

export default RootComponent;