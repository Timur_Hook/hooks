import React, {useState, useEffect, useRef} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Animated, TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const Useref = () => {
    const navigation = useNavigation();
    const counterObj = {counter: 0}
    const inputRef = useRef(null);


    const {buttonStyle, inputStyle} = styles;

    const increment = () => {
        inputRef.current.focus();
    }

return (
   <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
       <TouchableOpacity 
        style={buttonStyle}
        onPress={() => navigation.goBack()}>
           <Text style={{textAlign: 'center'}}>Назад</Text>
       </TouchableOpacity>
       <TextInput 
            ref={(ref) => inputRef.current = ref}
            style={inputStyle}
       />
       <TouchableOpacity 
        style={buttonStyle}
        onPress={increment}>
           <Text style={{textAlign: 'center', fontSize: 20}}>Фокус</Text>
       </TouchableOpacity>
   </View>
 )
}

const styles = StyleSheet.create({
    buttonStyle: {width:'80%', height: 40, borderWidth: 1, borderColor: 'blue', borderRadius: 5, justifyContent: 'center'},
    inputStyle: {
        height: 40,
        width: '80%',
        borderWidth: 1,
        borderColor: 'red',
        textAlign: 'center'
    }
});

export default Useref;