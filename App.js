import React from 'react';
import 'react-native-gesture-handler';
import RootComponent from './src/pages';
import {Provider} from 'react-redux';



const App = () => {
  return (
    <>
     <RootComponent />
    </>
  );
};



export default App;
